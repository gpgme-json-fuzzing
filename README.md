# Protobuf JSON Fuzzer

This project aims to enable effective fuzzing of JSON consumers with the help of
Protocol Buffers and libprotobuf-mutator.

## Dependencies

### General

You will need CMake starting from version 3.12 and clang starting from version 6.0.0
You will also need to install the protocol buffers library.

CMake can be installed like this:

```console
wget https://github.com/Kitware/CMake/releases/download/v3.13.1/cmake-3.13.1-Linux-x86_64.sh
chmod u+x cmake*.sh
./cmake*.sh
```

clang can be installed like this:


```console
wget http://releases.llvm.org/7.0.0/clang+llvm-7.0.0-x86_64-linux-gnu-ubuntu-16.04.tar.xz
tar -xJf clang+llvm-7.0.0-x86_64-linux-gnu-ubuntu-16.04.tar.xz
rm -rf /usr/local/bin/clang* /usr/local/lib/clang
cp -rf clang+llvm-7.0.0-x86_64-linux-gnu-ubuntu-16.04/bin/*  /usr/local/bin
cp -rf clang+llvm-7.0.0-x86_64-linux-gnu-ubuntu-16.04/lib/clang  /usr/local/lib
```

Protobuf:

```console
git clone https://github.com/protocolbuffers/protobuf.git
cd protobuf
git submodule update --init --recursive
./autogen.sh
./configure
make -j$(nproc)
make install
ldconfig
```


### Debian / Ubuntu

The installation was tested on Ubuntu 18.04, Ubuntu 18.10 and Debian 9.
The following packages have to be installed on the system:

```console
apt-get update && apt-get install -y \
 autoconf \
 automake \
 binutils \
 build-essential \
 curl \
 git \
 libc-dev \
 libtool \
 libboost-system-dev \
 libboost-thread-dev \
 unzip \
 make \
 wget \
 xz-utils \
 zlib1g-dev \
```

## Docker

The Dockerfile in this repo needs a private key in order to download from this
repository. In order not to leak this key in a docker image, an intermediate
container is used to download the repository. The files are then copied to the
final image.

You can build your docker image like this

```console
docker build --no-cache -t="protobuf-json-fuzzer" --build-arg SSH_PRIVATE_KEY="$(cat ~/.ssh/id_rsa)" .
```