#!/bin/bash
#  Copyright (C) 2018 by Bundesamt für Sicherheit in der Informationstechnik
#  Software engineering by Code Intelligence GmbH
#
#  Author(s): Christian Hartlage

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

parent_path=`pwd`
find "$parent_path/$1_proto" -type f -execdir bash -c "$parent_path/proto_to_json_converter < {} > $parent_path/$1_merged/{}" \;
