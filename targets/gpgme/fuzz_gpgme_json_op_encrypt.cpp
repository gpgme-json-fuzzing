//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

#include <cstdlib>
#include <string>
extern "C" {
#include "sample_keys.h"
#include "gpgme.h"
#include "cJSON.h"
#include "gpgme-json.h"
}

extern "C" int LLVMFuzzerTestOneInput(const char *data, size_t size) {
  if (size < 2) {
    return 0;
  }
  const auto flag_byte = (uint16_t)*data;

  static bool init = FuzzTargetInitialize();
  auto str = std::string(data, size);

  cjson_t response = cJSON_CreateObject();
  cjson_t request = cJSON_CreateObject();
//["bravo@example.net", "alpha@example.net"]
  request = cJSON_AddStringToObject(request, "op", "encrypt");
  request = cJSON_AddStringToObject(request, "keys", "alpha@example.net");
  request = cJSON_AddStringToObject(request, "data", str.c_str());

  request = cJSON_AddStringToObject(request, "protocol",   (flag_byte & 1) ? "openpgp" : "cms");
  request = cJSON_AddBoolToObject(request, "base64",        flag_byte & 1<<1);
  request = cJSON_AddBoolToObject(request, "mime",          flag_byte & 1<<2);
  request = cJSON_AddBoolToObject(request, "armor",         flag_byte & 1<<3);
  request = cJSON_AddBoolToObject(request, "always-trust",  flag_byte & 1<<4);
  request = cJSON_AddBoolToObject(request, "no-encrypt-to", flag_byte & 1<<5);
  request = cJSON_AddBoolToObject(request, "no-compress",   flag_byte & 1<<6);
  request = cJSON_AddBoolToObject(request, "throw-keyids",  flag_byte & 1<<7);
  request = cJSON_AddBoolToObject(request, "want-address",  flag_byte & 1<<8);
  request = cJSON_AddBoolToObject(request, "wrap",          flag_byte & 1<<9);

  op_encrypt(request, response);

  cJSON_Delete(request);
  cJSON_Delete(response);

  return 0;
}
