//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

#include <cstdlib>
#include <string>
extern "C" {
#include "sample_keys.h"
#include "gpgme.h"
#include "cJSON.h"
#include "gpgme-json.h"
}

extern "C" int LLVMFuzzerTestOneInput(const char *data, size_t size) {
  static bool init = FuzzTargetInitialize();
  auto str = std::string(data, size);

  cjson_t response = cJSON_CreateObject();
  cjson_t request = cJSON_CreateObject();

  request = cJSON_AddStringToObject(request, "op", "config");
  request = cJSON_AddStringToObject(request, "component", str.c_str());

  op_config(request, response);

  cJSON_Delete(request);
  cJSON_Delete(response);

  return 0;
}
