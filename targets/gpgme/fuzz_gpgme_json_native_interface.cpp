//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

extern "C" {
#include "sample_keys.h"
#include "cJSON.h"
#include "gpgme-json.h"
}

// To allow JSON fuzzing
#include "custom_mutators.h"

DEFINE_JSON_FUZZER(const char *json, size_t size) {
    static bool init = FuzzTargetInitialize();
    char *response = process_request(json);
    if (response) {
        //size_t error;
        //cJSON *root = cJSON_Parse(response, &error);
        //auto type = cJSON_GetObjectItem(root, "type");
        //auto msg = cJSON_GetObjectItem(root, "msg");
        //if (strncmp(type->valuestring, "error", 5) == 0 && std::string(msg->valuestring).find("invalid JSON object") != std::string::npos) {
        //    std::cout << response << std::endl << json << std::endl << "======" << std::endl;
        //}
        //cJSON_Delete(root);
        free(response);
    } else {
        std::cout << json << "+++++++" << std::endl;
        std::cout << std::string(json, size) << std::endl;
        std::cout << "===============================" << std::endl;
    }
}