//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Fuzzes the cppcms json parser using the protobuf JSON fuzzer
//
//===----------------------------------------------------------------------===//

#include <sstream>
#include <string>
#include <cstring>
#include "custom_mutators.h"

#include <gpgme.h>

#include "cJSON.h"

DEFINE_JSON_FUZZER(const char* json, size_t size) {
    size_t error;
    auto j1 = cJSON_Parse(json, &error);
    if(!error) {
        auto rendered = cJSON_Print(j1);
        auto j2 = cJSON_Parse(rendered, &error);
        if (!j2) {
            std::cout << "FAILED!" << std::endl;
            exit(1);
        }
        auto rendered2 = cJSON_Print(j2);
        if(std::string(rendered) != std::string(rendered2)) {
            std::cout << std::string(json, size) << std::endl <<rendered << std::endl << rendered2 << std::endl;
        }
        assert(std::string(rendered) == std::string(rendered2));

        free(rendered);
        free(rendered2);
        cJSON_Delete(j2);
    }
    cJSON_Delete(j1);
}
