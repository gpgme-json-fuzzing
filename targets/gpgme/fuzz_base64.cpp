//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Fuzzes the cppcms json parser using the protobuf JSON fuzzer
//
//===----------------------------------------------------------------------===//



#define GPGRT_ENABLE_ES_MACROS 1
#define GPGRT_ENABLE_LOG_MACROS 1
#define GPGRT_ENABLE_ARGPARSE_MACROS 1

#include <string.h>
#include <config.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>

extern "C" {

#include <gpgme.h>
#include <util.h>
#include "cJSON.h"
}


extern "C" int LLVMFuzzerTestOneInput(const char *data, size_t size) {

    struct b64state state{};
    size_t nbytes;

    // first try to decode...
    _gpgme_b64dec_start(&state, "");
    _gpgme_b64dec_proc(&state, (void *) data, size, &nbytes);
    _gpgme_b64dec_finish(&state);

    char *buffer;

    // then encode the random string in base64
    auto fp = es_fopenmem(0, "rwb");
    auto enc_state = gpgrt_b64enc_start(fp, "");
    gpgrt_b64enc_write(enc_state, data, size);
    gpgrt_b64enc_finish(enc_state);

    es_fputc(0, fp);
    es_fclose_snatch(fp, (void **) &buffer, nullptr);

    // then decode again...
    state = b64state{};
    _gpgme_b64dec_start(&state, "");
    _gpgme_b64dec_proc(&state, buffer, strlen(buffer), &nbytes);
    _gpgme_b64dec_finish(&state);

    free(buffer);
    return 0;
}

