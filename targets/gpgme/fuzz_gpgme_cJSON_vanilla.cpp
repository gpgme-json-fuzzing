//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Fuzzes the cppcms json parser using the protobuf JSON fuzzer
//
//===----------------------------------------------------------------------===//

#include <iostream>
#include <string>

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <gpgme.h>

#include "cJSON.h"

extern "C" int LLVMFuzzerTestOneInput(const char *data, size_t size) {
    std::string str = std::string(data, size);
    size_t erroff;
    auto j1 = cJSON_Parse(str.c_str(), &erroff);
    if(j1) {
        auto rendered = cJSON_Print(j1);
        auto j2 = cJSON_Parse(rendered, &erroff);
        if (!j2) {
            std::cout << "FAILED!" << std::endl;
            exit(1);
        }
        auto rendered2 = cJSON_Print(j2);
        if(std::string(rendered) != std::string(rendered2)) {
            std::cout << std::string(data, size) << std::endl <<rendered << std::endl << rendered2 << std::endl;
        }
        assert(std::string(rendered) == std::string(rendered2));

        free(rendered);
        free(rendered2);
        cJSON_Delete(j2);
    }
    cJSON_Delete(j1);

    return 0;
}
