//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//
// Created by Sergej Dechand on 12/13/18.
//
extern "C" {
#include "gpgme.h"
#include "sample_keys.h"
#include "cJSON.h"
#include "gpgme-json.h"
}

// To allow JSON fuzzing
#include "custom_mutators.h"


extern "C" int LLVMFuzzerTestOneInput(const char *data, size_t size) {
    static bool init = FuzzTargetInitialize();
    cjson_t json = cJSON_CreateObject();
    std::string str = {data, size};
    cJSON_AddStringToObject(json, "op", "keylist");
    auto keys = cJSON_CreateStringArray(&data, 1);
    cJSON_AddStringToObject(json, "userid", str.c_str());
    cJSON_AddStringToObject(json, "algo", "rsa1024");

    char *json_str = cJSON_Print(json);

    char *response = process_request(json_str);
    free(json_str);
    if (response) {
        free(response);
    }
    cJSON_Delete(json);
    return 0;
}
