//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

#include <cstdlib>
#include <string>
extern "C" {
#include "sample_keys.h"
#include "gpgme.h"
#include "cJSON.h"
#include "gpgme-json.h"
#include <util.h>
}

class SignVerifyInput {
 public:
  SignVerifyInput(const char *data, size_t size) : data(data, size), flag((uint16_t) *data) {}

  const char* c_str() { return data.c_str(); }
  const size_t size() { return data.size(); }

  const char* protocol() { return (flag & 1) ? "openpgp" : "cms"; }

  bool base64()        { return (flag & 1<<1) != 0; }
  bool mime()          { return (flag & 1<<2) != 0; }
  bool armor()         { return (flag & 1<<3) != 0; }
  bool always_trust()  { return (flag & 1<<4) != 0; }
  bool no_encrypt_to() { return (flag & 1<<5) != 0; }
  bool no_compress()   { return (flag & 1<<6) != 0; }
  bool throw_keyids()  { return (flag & 1<<7) != 0; }
  bool want_address()  { return (flag & 1<<8) != 0; }
  bool wrap()          { return (flag & 1<<9) != 0; }

 private:
  std::string data;
  uint16_t flag;
};

extern "C" int LLVMFuzzerTestOneInput(const char *data, size_t size) {
  static bool init = FuzzTargetInitialize();

  cJSON *encJSON, *decJSON, *decrypt_res_JSON, *enc_res_JSON;
  char *encrypt_req, *encrypt_res, *enc_data, *decrypt_res, *decrypt_req, *decrypted_plaintext;
  bool dec_data_base64, enc_data_base64;

  size_t r_erroff;
  struct b64state state;
  size_t nbytes;
  if (size < 2) return 0;

  SignVerifyInput input(data, size);

  encJSON = cJSON_CreateObject();
  cJSON_AddStringToObject(encJSON, "op", "encrypt");
  cJSON_AddStringToObject(encJSON, "keys", "alpha@example.net");
  cJSON_AddStringToObject(encJSON, "data", input.c_str());

  std::string protocol{};
  cJSON_AddStringToObject(encJSON, "protocol", input.protocol());
  cJSON_AddBoolToObject(encJSON, "base64", input.base64());
  cJSON_AddBoolToObject(encJSON, "mime", input.mime());
  cJSON_AddBoolToObject(encJSON, "armor", input.armor());
  cJSON_AddBoolToObject(encJSON, "always-trust", input.always_trust());
  cJSON_AddBoolToObject(encJSON, "no-encrypt-to", input.no_encrypt_to());
  cJSON_AddBoolToObject(encJSON, "no-compress", input.no_compress());
  cJSON_AddBoolToObject(encJSON, "throw-keyids", input.throw_keyids());
  cJSON_AddBoolToObject(encJSON, "want-address", input.want_address());
  cJSON_AddBoolToObject(encJSON, "wrap", input.wrap());

  encrypt_req = cJSON_Print(encJSON);
//  if (encrypt_req == NULL) goto cleanup_before_encrypt_request;

  encrypt_res = process_request(encrypt_req);
  if (encrypt_res == NULL) goto cleanup_before_encrypt_request;

  // Encrypt was successful
  enc_res_JSON = cJSON_Parse(encrypt_res, &r_erroff);
  enc_data = cJSON_Print(cJSON_GetObjectItem(enc_res_JSON, "data"));
  if (enc_data == NULL) goto cleanup_after_encrypt_request;
  enc_data_base64 = cjson_is_true(cJSON_GetObjectItem(enc_res_JSON, "base64"));

  decJSON = cJSON_CreateObject();
  cJSON_AddStringToObject(decJSON, "op", "decrypt");
  cJSON_AddStringToObject(decJSON, "data", enc_data);
  cJSON_AddStringToObject(decJSON, "protocol", input.protocol());
  cJSON_AddBoolToObject  (decJSON, "base64", enc_data_base64);

  decrypt_req = cJSON_Print(decJSON);
  decrypt_res = process_request(decrypt_req);
  if (decrypt_res == NULL) goto cleanup_before_decrypt_request;

  decrypt_res_JSON = cJSON_Parse(decrypt_res, &r_erroff);
  if (!decrypt_res_JSON) goto cleanup_before_decrypt_request;

  decrypted_plaintext = cJSON_Print(cJSON_GetObjectItem(decrypt_res_JSON, "data"));
  if (!decrypted_plaintext) goto cleanup_after_decrypt_request;
  dec_data_base64 = cjson_is_true(cJSON_GetObjectItem(decrypt_res_JSON, "base64"));

  if (dec_data_base64) {
    _gpgme_b64dec_start(&state, "");
    _gpgme_b64dec_proc(&state, decrypted_plaintext, input.size(), &nbytes);
    _gpgme_b64dec_finish(&state);
  }

  if (strncmp(decrypted_plaintext, input.c_str(), input.size()) != 0) {
    abort();
  }

cleanup_after_decrypt_request:
  free(decrypted_plaintext);
  cJSON_Delete(decrypt_res_JSON);

cleanup_before_decrypt_request:
  free(decrypt_req);
  cJSON_Delete(decJSON);

cleanup_after_encrypt_request:
  free(enc_data);
  cJSON_Delete(enc_res_JSON);

cleanup_before_encrypt_request:
  free(encrypt_req);
  cJSON_Delete(encJSON);

  return 0;
}
