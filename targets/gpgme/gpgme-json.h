//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//
// Created by user on 12/12/18.
//

#ifndef PROTOBUF_JSON_FUZZER_GPGME_STATIC_H
#define PROTOBUF_JSON_FUZZER_GPGME_STATIC_H

char *process_request (const char *request);
gpg_error_t op_config (cjson_t request, cjson_t result);
gpg_error_t op_encrypt (cjson_t request, cjson_t result);

#endif //PROTOBUF_JSON_FUZZER_GPGME_STATIC_H
