//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

#include <cstdlib>
#include <string>
extern "C" {
#include "sample_keys.h"
#include "gpgme.h"
#include "cJSON.h"
#include "gpgme-json.h"
#include <util.h>
}

class SignVerifyInput {
 public:
  SignVerifyInput(const char *data, size_t size) : data(data, size), flag((uint16_t) *data) {}

  const char* c_str() { return data.c_str(); }
  const size_t size() { return data.size(); }

  const char* protocol() { return (flag & 1) ? "openpgp" : "cms"; }

  bool base64()        { return (flag & 1<<1) != 0; }
  bool armor()         { return (flag & 1<<2) != 0; }

 private:
  std::string data;
  uint16_t flag;
};

extern "C" int LLVMFuzzerTestOneInput(const char *data, size_t size) {
  static bool init = FuzzTargetInitialize();

  cJSON *sign_JSON, *verify_JSON, *verify_res_JSON, *sign_res_JSON;
  char *sign_req, *sign_res, *signature, *verify_res, *verify_req, *verified_plaintext;
  bool dec_data_base64, signature_is_base64;

  size_t r_erroff;
  struct b64state state;
  size_t nbytes;
  if (size < 2) return 0;

  SignVerifyInput input(data, size);

  sign_JSON = cJSON_CreateObject();
  cJSON_AddStringToObject(sign_JSON, "op", "sign");
  cJSON_AddStringToObject(sign_JSON, "keys", "alpha@example.net");
  cJSON_AddStringToObject(sign_JSON, "data", input.c_str());

  std::string protocol{};
  cJSON_AddStringToObject(sign_JSON, "protocol", input.protocol());
  cJSON_AddBoolToObject(sign_JSON, "base64", input.base64());
  cJSON_AddBoolToObject(sign_JSON, "armor", input.armor());

  sign_req = cJSON_Print(sign_JSON);
//  if (sign_req == NULL) goto cleanup_before_encrypt_request;

  sign_res = process_request(sign_req);
  if (sign_res == NULL) goto cleanup_before_encrypt_request;

  // Encrypt was successful
  sign_res_JSON = cJSON_Parse(sign_res, &r_erroff);
  signature = cJSON_Print(cJSON_GetObjectItem(sign_res_JSON, "data"));
  if (signature == NULL) goto cleanup_after_encrypt_request;
  signature_is_base64 = cjson_is_true(cJSON_GetObjectItem(sign_res_JSON, "base64"));

  verify_JSON = cJSON_CreateObject();
  cJSON_AddStringToObject(verify_JSON, "op", "verify");
  cJSON_AddStringToObject(verify_JSON, "signature", signature);
  cJSON_AddStringToObject(verify_JSON, "protocol", input.protocol());
  cJSON_AddBoolToObject  (verify_JSON, "base64", signature_is_base64);

  verify_req = cJSON_Print(verify_JSON);
  verify_res = process_request(verify_req);
  if (verify_res == NULL) goto cleanup_before_decrypt_request;

  verify_res_JSON = cJSON_Parse(verify_res, &r_erroff);
  if (!verify_res_JSON) goto cleanup_before_decrypt_request;

  verified_plaintext = cJSON_Print(cJSON_GetObjectItem(verify_res_JSON, "data"));
  if (!verified_plaintext) goto cleanup_after_decrypt_request;
  dec_data_base64 = cjson_is_true(cJSON_GetObjectItem(verify_res_JSON, "base64"));

  if (dec_data_base64) {
    _gpgme_b64dec_start(&state, "");
    _gpgme_b64dec_proc(&state, verified_plaintext, input.size(), &nbytes);
    _gpgme_b64dec_finish(&state);
  }

  if (strncmp(verified_plaintext, input.c_str(), input.size()) != 0) {
    abort();
  }

cleanup_after_decrypt_request:
  free(verified_plaintext);
  cJSON_Delete(verify_res_JSON);

cleanup_before_decrypt_request:
  free(verify_req);
  cJSON_Delete(verify_JSON);

cleanup_after_encrypt_request:
  free(signature);
  cJSON_Delete(sign_res_JSON);

cleanup_before_encrypt_request:
  free(sign_req);
  cJSON_Delete(sign_JSON);

  return 0;
}
