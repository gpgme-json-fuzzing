# Copyright 2018 Code Intelligence GmbH All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

message("BUILDING gpgme")
set(GPGME_TARGET external.gpgme)
set(GPGME_INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/${GPGME_TARGET})
set(GPGME_SRC_DIR ${GPGME_INSTALL_DIR}/src/${GPGME_TARGET})

set(GPGME_INCLUDE_DIRS ${GPGME_INSTALL_DIR}/include/)
message(GPGME_INCLUDE_DIRS)
set(GPGME_INCLUDE_SRC ${GPGME_INSTALL_DIR}/src/${GPGME_TARGET}/src/)
set(GPGME_CONFIG ${GPGME_INSTALL_DIR}/src/${GPGME_TARGET}/conf/)
include_directories(${GPGME_INCLUDE_DIRS} ${GPGME_INCLUDE_SRC} ${GPGME_CONFIG})

list(APPEND GPGME_LIBRARIES gpgme)

foreach(lib IN LISTS GPGME_LIBRARIES)
    list(APPEND GPGME_BUILD_BYPRODUCTS ${GPGME_INSTALL_DIR}/lib/lib${lib}.so)

    add_library(${lib} SHARED IMPORTED)
    set_property(TARGET ${lib} PROPERTY IMPORTED_LOCATION
            ${GPGME_INSTALL_DIR}/lib/lib${lib}.so)
    add_dependencies(${lib} ${GPGME_TARGET})
endforeach(lib)

list(APPEND GPGME_OBJECTS_STATIC cJSON.o)
foreach(lib IN LISTS GPGME_OBJECTS_STATIC)
    list(APPEND GPGME_BUILD_BYPRODUCTS ${GPGME_INSTALL_DIR}/src/${GPGME_TARGET}/src/${lib})

    add_library(${lib} STATIC IMPORTED)
    set_property(TARGET ${lib} PROPERTY IMPORTED_LOCATION
            ${GPGME_INSTALL_DIR}/src/${GPGME_TARGET}/src/${lib})
    add_dependencies(${lib} ${GPGME_TARGET})
endforeach(lib)

list(APPEND GPGME_LIBRARIES_STATIC b64dec.o)
foreach(lib IN LISTS GPGME_LIBRARIES_STATIC)
    list(APPEND GPGME_BUILD_BYPRODUCTS ${GPGME_INSTALL_DIR}/src/${GPGME_TARGET}/src/${lib})

    add_library(${lib} STATIC IMPORTED)
    set_property(TARGET ${lib} PROPERTY IMPORTED_LOCATION
            ${GPGME_INSTALL_DIR}/src/${GPGME_TARGET}/src/.libs/${lib})
    add_dependencies(${lib} ${GPGME_TARGET})
endforeach(lib)

message(STATUS "CMAKE CONFIGURATION FOR GPGME")
message(STATUS "CMAKE_COMMAND ${CMAKE_COMMAND}")
message(STATUS "GPGME_TARGET ${GPGME_TARGET}")
message(STATUS "GPGME_INSTALL_DIR ${GPGME_INSTALL_DIR}")
message(STATUS "CMAKE_GENERATOR ${CMAKE_GENERATOR}")
message(STATUS "CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE}")
message(STATUS "CMAKE_C_COMPILER ${CMAKE_C_COMPILER}")
message(STATUS "CMAKE_CXX_COMPILER ${CMAKE_CXX_COMPILER}")
message(STATUS "CMAKE_C_FLAGS=${CMAKE_C_FLAGS} ${FUZZING_FLAGS}")
message(STATUS "CMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} ${FUZZING_FLAGS}")
message(STATUS "GPGME_LIBRARIES=${GPGME_LIBRARIES}")

set(GPGME_CFLAGS "-g -O0 -fsanitize=address,undefined,fuzzer-no-link -fsanitize-coverage=trace-cmp,trace-gep  -fno-omit-frame-pointer")
set(GPGME_CXXFLAGS "-g -O0 -fsanitize=address,undefined,fuzzer-no-link -fsanitize-coverage=trace-cmp,trace-gep -fno-omit-frame-pointer")

include (ExternalProject)
ExternalProject_Add(${GPGME_TARGET}
        PREFIX ${GPGME_TARGET}
        GIT_REPOSITORY https://github.com/gpg/gpgme.git
        GIT_TAG master
        UPDATE_COMMAND ""
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND ./autogen.sh
        COMMAND ASAN_OPTIONS=detect_leaks=0 ./configure --prefix=${GPGME_INSTALL_DIR} --disable-gpg-test --disable-glibtest --disable-gpgconf-test --disable-g13-test --disable-gpgsm-test CC=clang CFLAGS=${GPGME_CFLAGS} CXX=clang++ CXXFLAGS=${GPGME_CXXFLAGS}
        BUILD_BYPRODUCTS ${GPGME_BUILD_BYPRODUCTS}
        BUILD_COMMAND $(MAKE) all
        )

set(GPG)
