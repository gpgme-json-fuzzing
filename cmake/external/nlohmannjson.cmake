# Copyright 2017 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set(NLOHMANNJSON_TARGET external.nlohmannjson)
set(NLOHMANNJSON_INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/${NLOHMANNJSON_TARGET})
set(NLOHMANNJSON_SRC_DIR ${NLOHMANNJSON_INSTALL_DIR}/src/${NLOHMANNJSON_TARGET})

set(NLOHMANNJSON_INCLUDE_DIRS ${NLOHMANNJSON_INSTALL_DIR}/include/)
include_directories(${NLOHMANNJSON_INCLUDE_DIRS})

message(STATUS "CMAKE CONFIGURATION FOR NLOHMANNJSON")
message(STATUS "CMAKE_COMMAND ${CMAKE_COMMAND}")
message(STATUS "NLOHMANNJSON_TARGET ${NLOHMANNJSON_TARGET}")
message(STATUS "NLOHMANNJSON_INSTALL_DIR ${NLOHMANNJSON_INSTALL_DIR}")
message(STATUS "CMAKE_GENERATOR ${CMAKE_GENERATOR}")
message(STATUS "CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE}")
message(STATUS "CMAKE_C_COMPILER ${CMAKE_C_COMPILER}")
message(STATUS "CMAKE_CXX_COMPILER ${CMAKE_CXX_COMPILER}")
message(STATUS "CMAKE_C_FLAGS=${CMAKE_C_FLAGS} ${FUZZING_FLAGS}")
message(STATUS "CMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} ${FUZZING_FLAGS}")

include (ExternalProject)
ExternalProject_Add(${NLOHMANNJSON_TARGET}
        PREFIX ${NLOHMANNJSON_TARGET}
        GIT_REPOSITORY https://github.com/nlohmann/json.git
        GIT_TAG master
        UPDATE_COMMAND ""
        CONFIGURE_COMMAND ${CMAKE_COMMAND} ${NLOHMANNJSON_SRC_DIR}
        -G${CMAKE_GENERATOR}
        -DCMAKE_INSTALL_PREFIX=${NLOHMANNJSON_INSTALL_DIR}
        -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
        -DJSON_BuildTests=OFF
        BUILD_COMMAND $(MAKE) -j ${CPU_COUNT} all
        )