//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Stringification of an protobuf Element object
//
//===----------------------------------------------------------------------===//

#include <json_proto.pb.h>
#include "proto_to_json.h"
#include "json_string.h"

std::string ElementToString (const Element& element){
    JsonString json;
    json.append(element);
    return json.str();
}

std::string ProtoToJson(const uint8_t *data, size_t size){
    Element element;
    if (!element.ParsePartialFromArray(data, size))
        return "#error invalid proto\n";
    return ElementToString(element);
}

