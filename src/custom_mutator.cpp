//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//
// Created by Sergej Dechand on 12/11/18.
//

#include "custom_mutators.h"

const std::string* LoadJsonInput(const uint8_t *data, size_t size) {
    static JsonString json;
    Element input;
    if(input.ParsePartialFromString(std::string((const char*) data, size))) {
        json.clear();
        json.append(input);
        return &json.str();
    }
    return nullptr;
}

