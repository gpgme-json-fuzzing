//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Takes json file names as arguments and prints them out as protobuf
//
//===----------------------------------------------------------------------===//
#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>
#include "json_to_proto.h"

int main(int argc, char **argv) {
    for (int i = 1; i < argc; i++) {
        std::fstream in(argv[i]);
        std::string str((std::istreambuf_iterator<char>(in)),
                         std::istreambuf_iterator<char>());
        Element element;
        if(JsonToProto(reinterpret_cast<const uint8_t *>(str.data()), str.size(), &element)) {
            element.SerializeToOstream(&std::cout);
        }
    }
    return 0;
}