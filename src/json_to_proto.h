//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
#ifndef PROTOBUF_JSON_FUZZER_JSON_TO_PROTO_H
#define PROTOBUF_JSON_FUZZER_JSON_TO_PROTO_H
#include <json_proto.pb.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

bool JsonToProto(const uint8_t *data, size_t size, Element *element);
void assignValue(Value* value, const json& j);
#endif //PROTOBUF_JSON_FUZZER_JSON_TO_PROTO_H
