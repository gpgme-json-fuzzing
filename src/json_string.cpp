//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Stringification of an protobuf Element object
//
//===----------------------------------------------------------------------===//

#include <ostream>
#include <sstream>
#include <iostream>
#include <fstream>
#include <json_proto.pb.h>
#include "json_string.h"


JsonString::JsonString() {
    json_string.reserve(4096 * 4096);
}

void JsonString::clear() {
    json_string.clear();
}

inline JsonString &JsonString::append(const std::string &str) {
    json_string.append(str);
    return *this;
}

inline JsonString &JsonString::append(const char c) {
    json_string.push_back(c);
    return *this;
}

JsonString &JsonString::append(const Literal &x) {
    switch (x.lit()) {
        case Literal::LIT_FALSE:
            append("false");
            break;
        case Literal::LIT_TRUE:
            append("true");
            break;
        default:
            break;
    }
    return *this;
}

JsonString &JsonString::append(const Number &x) {
    google::protobuf::int32 val = x.integer();
    google::protobuf::int32 frac = x.fraction();
    google::protobuf::int32 exp = x.exponent();

    append(std::to_string(val));
    if (frac > 0) {
        append('.').append(std::to_string(frac));
    }
    if (exp % 2) {
        append('e').append(std::to_string(exp));
    }
    return *this;
}

JsonString &JsonString::append(const Array &x) {
    append('[');
    for (auto it = x.values().begin(); it != x.values().end(); ++it) {
        append(*it);
        if (std::next(it) != x.values().end()) {
            append(',');
        }
    }
    return append(']');
}

bool ToASCII(char *Data, size_t Size) {
  bool Changed = false;
  for (size_t i = 0; i < Size; i++) {
    char &X = Data[i];
    auto NewX = X;
    NewX &= 127;
    if (!isspace(NewX) && !isprint(NewX))
      NewX = ' ';
    Changed |= NewX != X;
    X = NewX;
  }
  return Changed;
}

std::string escape_json(const std::string &s) {
    std::ostringstream o;
    for (auto c = s.cbegin(); c != s.cend(); c++) {
        switch (*c) {
        case '"': o << "\\\""; break;
        case '\0': o << "\\"; break;
        case '\\': o << "\\\\"; break;
        case '\b': o << "\\b"; break;
        case '\f': o << "\\f"; break;
        case '\n': o << "\\n"; break;
        case '\r': o << "\\r"; break;
        case '\t': o << "\\t"; break;
        default:
                o << *c;
        }
    }
    return o.str();
}

JsonString &JsonString::append(const String &x) {
    std::string str = x.val();
    append('"');

    //if (str.find(char(0)) != std::string::npos) {
    //std::replace(str.begin(), str.end(), '\0', '\\');
    //}
    //ToASCII((char*) str.c_str(), (size_t) str.size());
    str = escape_json(str);
    append(str);
    return append('"');
}

JsonString &JsonString::append(const KeyValPair &x) {
    return append(x.key()).append(':').append(x.value());
}

JsonString &JsonString::append(const Object &x) {
    append('{');
    for (auto it = x.keyvalpairs().begin(); it != x.keyvalpairs().end(); ++it) {
        append(*it);
        if (std::next(it) != x.keyvalpairs().end()) {
            append(',');
        }
    }
    return append('}');
}


JsonString &JsonString::append(const Value &x) {
    switch (x.value_oneof_case()) {
        case Value::kObject:
            append(x.object());
            break;
        case Value::kArray:
            append(x.array());
            break;
        case Value::kString:
            append(x.string());
            break;
        case Value::kNumber:
            append(x.number());
            break;
        case Value::kLiteral:
            append(x.literal());
            break;
        default:
            append("null");
    }
    return *this;
}

JsonString &JsonString::append(const Element &x) {
    return append(x.value());
}

const std::string &JsonString::str() const {
    return json_string;
}
