//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Takes json from stdin and prints the protobuf to stdout
//
//===----------------------------------------------------------------------===//
#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>
#include "json_to_proto.h"

int main(int argc, char **argv) {
    std::string str((std::istreambuf_iterator<char>(std::cin)),
                     std::istreambuf_iterator<char>());
    Element* input;
    JsonToProto(reinterpret_cast<const uint8_t *>(str.data()), str.size(), input);
    input->SerializeToOstream(&std::cout);
    return 0;
}
