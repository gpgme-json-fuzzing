//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//
// Created by Sergej Dechand on 12/7/18.
//

#ifndef PROTOBUF_JSON_FUZZER_CUSTOM_MUTATORS_H
#define PROTOBUF_JSON_FUZZER_CUSTOM_MUTATORS_H

#include <string>
#include "json_string.h"
#include "json_proto.pb.h"
#include "json_to_proto.h"
#include "src/libfuzzer/libfuzzer_macro.h"
#include "src/binary_format.h"
#include "src/random.h"
#include "src/mutator.h"

#include "json_string.h"

#include "port/protobuf.h"

const std::string* LoadJsonInput(const uint8_t *data, size_t size);

#define DEFINE_TEST_ONE_JSON_INPUT_IMPL                                     \
  extern "C" int LLVMFuzzerTestOneInput(const uint8_t* data, size_t size) { \
    if (size < 20) return 0; \
    if (auto json = LoadJsonInput(data, size)) {                            \
      TestOneProtoInput(json->c_str(), json->size());                       \
    }                                                                       \
    return 0;                                                               \
  }

#define DEFINE_CUSTOM_JSON_MUTATOR_IMPL(use_binary, Proto)                     \
  extern "C" size_t LLVMFuzzerCustomMutator(                                   \
      uint8_t* data, size_t size, size_t max_size, unsigned int seed) {        \
    using protobuf_mutator::libfuzzer::CustomProtoMutator;                     \
    Proto input;                                                               \
    return CustomProtoMutator(use_binary, data, size, max_size, seed, &input); \
  }

#define DEFINE_JSON_FUZZER(arg, size)                                  \
  static void TestOneProtoInput(arg, size);                            \
  DEFINE_CUSTOM_JSON_MUTATOR_IMPL(true, Element)                       \
  DEFINE_CUSTOM_PROTO_CROSSOVER_IMPL(true, Element)                    \
  DEFINE_TEST_ONE_JSON_INPUT_IMPL                                      \
  static void TestOneProtoInput(arg, size)

#endif //PROTOBUF_JSON_FUZZER_CUSTOM_MUTATORS_H
