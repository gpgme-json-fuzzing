//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#ifndef JSON_PROTO_PROTO_TO_JSON_H
#define JSON_PROTO_PROTO_TO_JSON_H

#include <iostream>
#include "json_proto.pb.h"

std::string ElementToString(const Element &input);
std::string ProtoToJson(const uint8_t *data, size_t size);

#endif //JSON_PROTO_PROTO_TO_JSON_H
