//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Takes protobuf from stdin and prints the json to stdout
//
//===----------------------------------------------------------------------===//

#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>
#include "proto_to_json.h"

using namespace std;

int main(int argc, char **argv) {
    std::string str((std::istreambuf_iterator<char>(std::cin)),
                    std::istreambuf_iterator<char>());
    std::cout << ProtoToJson(
            reinterpret_cast<const uint8_t *>(str.data()), str.size()) << std::endl;
    return 0;
}
