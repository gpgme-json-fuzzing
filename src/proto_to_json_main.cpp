//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Takes protobuf Elements file names as arguments and prints them out
//
//===----------------------------------------------------------------------===//

#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>
#include "proto_to_json.h"

int main(int argc, char **argv) {
    for (int i = 1; i < argc; i++) {
        std::fstream in(argv[i]);
        std::string str((std::istreambuf_iterator<char>(in)),
                        std::istreambuf_iterator<char>());
        std::cout << ProtoToJson(
                reinterpret_cast<const uint8_t *>(str.data()), str.size());
        std::cout << " //" << argv[i] << std::endl << std::endl;

    }
}
