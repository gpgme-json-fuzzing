//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//

#ifndef JSON_STRING
#define JSON_STRING

#include <iostream>
#include <string>
#include "json_proto.pb.h"

class JsonString {
private:
    std::string json_string;

public:
    JsonString();
    JsonString& append(const std::string&);
    JsonString& append(char);
    JsonString& append(const Literal&);
    JsonString& append(const Number&);
    JsonString& append(const Array&);
    JsonString& append(const String&);
    JsonString& append(const KeyValPair&);
    JsonString& append(const Object&);
    JsonString& append(const Value&);
    JsonString& append(const Element&);

    void clear();
    const std::string& str() const;
};

#endif //JSON_STRING
