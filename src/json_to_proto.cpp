//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  converts json to protbuf element
//
//===----------------------------------------------------------------------===//

#include <ostream>
#include <sstream>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <json_proto.pb.h>
#include "json_to_proto.h"

bool JsonToProto(const uint8_t *data, size_t size, Element *element) {

    nlohmann::json j;
    try {
        j = json::parse(data, data + size);
    } catch (json::parse_error& e) {
         return false;
     }

    if (j.type() != json::value_t::discarded) {
        Value *value = element->mutable_value();
        assignValue(value, j);
        return true;
    }
    return false;
}

void assignValue(Value *value, const json& json){

    switch (json.type()){
        case json::value_t::discarded: {

            break;
        }

        case json::value_t::null: {

            break;
        }

        case json::value_t::boolean: {
            if (json.get<bool>()){
                value->mutable_literal()->set_lit(Literal_Lit_LIT_TRUE);
            } else {
                value->mutable_literal()->set_lit(Literal_Lit_LIT_FALSE);
            }
            break;
        }

        case json::value_t::string: {
            value->mutable_string()->set_val(json.get<std::string>());
            break;
        }

        case json::value_t::number_integer: {
            value->mutable_number()->set_integer(json.get<int32_t >());
            break;
        }

        case json::value_t::number_unsigned:{
            value->mutable_number()->set_integer(json.get<uint32_t >());
            break;
        }

        case json::value_t::number_float: {
            //TODO floats and exponential numbers
            break;
        }

        case json::value_t::object: {
            Object* object = value->mutable_object();
            for (const auto& it: json.items()){
                KeyValPair* kvp = object->add_keyvalpairs();
                kvp->mutable_key()->set_val(it.key());
                assignValue(kvp->mutable_value(), it.value());
            }
            break;
        }

        case json::value_t::array: {
            Array* array = value->mutable_array();
            for (const auto& it: json.items()){
                assignValue(array->add_values(), it.value());
            }
            break;
        }
    }
}
