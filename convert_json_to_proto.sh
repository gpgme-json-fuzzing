#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

parent_path=`pwd`
find "$parent_path/$1_json" -type f -execdir bash -c "$parent_path/json_to_proto_main {} > $parent_path/$1_protobuf/{}" \;
