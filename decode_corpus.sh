#!/bin/bash
#  Copyright (C) 2018 by Bundesamt für Sicherheit in der Informationstechnik
#  Software engineering by Code Intelligence GmbH
#
#  Author(s): Sergej Dechand, Christian Hartlage

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd "$parent_path"

for var in "$@"
do
    echo "$var"
    protoc --decode Element --proto_path "$parent_path/src" json_proto.proto < "$var"
    echo -e "\n\n"
done

