//  Copyright (C) 2019 by Bundesamt für Sicherheit in der Informationstechnik
//  Software engineering by Code Intelligence GmbH
//
//  Author(s): Sergej Dechand, Christian Hartlage

//                         Protobuf JSON Fuzzer
//
// This file is distributed under the MIT License
// See LICENSE for details.
//
//===----------------------------------------------------------------------===//
//
//  Tests whether the fuzzer finds the basic string "foobarliciouslongword"
//
//===----------------------------------------------------------------------===//

#include <cstdint>
#include <fstream>
#include "src/mutator.h"
#include "json_proto.pb.h"
#include "src/libfuzzer/libfuzzer_macro.h"
#include "../src/proto_to_json.h"


DEFINE_BINARY_PROTO_FUZZER(const Element& x) {

    if (!x.has_value())
        return;
    if (!x.value().has_string())
        return;
    if (x.value().string().val() != "foobarliciouslongword")
        return;

    std::cerr << "DEBUGGING\n";
    std::cerr << ElementToString(x) << "\n";
    std::cerr << "DEBUGGING END\n";
    __builtin_trap();
}
