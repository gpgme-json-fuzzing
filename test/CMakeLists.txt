include_directories(${protobuf-json-fuzzer_SOURCE_DIR}/src)
include_directories(${CMAKE_BINARY_DIR}/src)

set (TESTS
    string
    simple_array
    simple_object
        )

#foreach (test ${TESTS})
#
#    add_executable(${test}_test ${test}_test.cpp)
#    target_link_libraries(${test}_test
#            ${PROTOBUF_LIBRARIES}
#            protobuf-json
#            protobuf-mutator
#            protobuf-mutator-libfuzzer
#            -fsanitize=address -fno-omit-frame-pointer
#            )
#    target_compile_options(${test}_test PUBLIC -fsanitize=address -fno-omit-frame-pointer)
#
#    add_test(NAME ${test}_test COMMAND ${test}_test -error_exitcode=0)
#endforeach()

