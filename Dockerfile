#  Copyright (C) 2018 by Bundesamt für Sicherheit in der Informationstechnik
#  Software engineering by Code Intelligence GmbH
#
#  Author(s): Sergej Dechand, Christian Hartlage

# use an intermediate container to clone a private git repo without leaking the private key
# provide your SSH_PRIVATE KEY while building the container like this
# docker build --build-arg SSH_PRIVATE_KEY="$(cat ~/.ssh/id_rsa)" .
FROM debian:stretch as intermediate

WORKDIR /code
# install some dependencies
RUN apt-get update && apt-get install --no-install-recommends -y software-properties-common gnupg2

# add official llvm apt repository
RUN apt-add-repository 'deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch main'
RUN apt-add-repository 'deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-6.0 main'
RUN apt-add-repository 'deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-7 main'
RUN apt-add-repository 'deb http://ftp.debian.org/debian stretch-backports main'

ADD llvm-snapshot.gpg.key /code
# no need to verify fingerprint since the key has been checked before putting into repo
RUN apt-key add /code/llvm-snapshot.gpg.key

# install basic build dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
 autoconf \
 automake \
 binutils \
 build-essential \
 curl \
 git \
 libc-dev \
 libtool \
 libboost-system-dev \
 libboost-thread-dev \
 make \
 texinfo \
 unzip \
 wget \
 xz-utils \
 zlib1g-dev

# install newer versions of protobuf and cmake from debian backports
RUN apt-get -t stretch-backports install -y cmake \
 libprotobuf-dev \
 protobuf-c-compiler  \
 protobuf-compiler \
 gnupg2 \
 gnupg \
 gnupg-agent \
 libassuan-dev \
 libgpg-error-dev

# install dependecies for gpgme and fuzzing
RUN apt-get install --install-recommends -y \
 clang-6.0 \
 clang-tools-6.0 \
 libclang-common-6.0-dev libclang-6.0-dev \
 libfuzzer-6.0-dev

RUN rm -rf /var/lib/apt/lists/*

RUN ln -s /usr/bin/clang-6.0 /usr/bin/clang
RUN ln -s /usr/bin/clang++-6.0 /usr/bin/clang++

COPY . /code/

# run cmake and build the project
RUN mkdir -p  /code/cmake-build

RUN cd  /code/cmake-build \
&& cmake -DCMAKE_BUILD_TYPE=Debug \
 -DCMAKE_C_COMPILER=clang \
 -DCMAKE_CXX_COMPILER=clang++ \
 -DLIB_PROTO_MUTATOR_TESTING=OFF \
 .. \
&& make -j$(nproc) all \
&& make -j$(nproc) fuzzers
